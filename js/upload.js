document.getElementById('download').addEventListener('click', () => {
   // Use Promise instead of callback function
   chrome.tabs.query({active: true, currentWindow: true}).then(tabs => {
   let currentTab = tabs[0];
   if (!currentTab.url.startsWith("https://ebridge.xjtlu.edu.cn/")) {
     return;
    }
    chrome.scripting.executeScript({
     target: {tabId: currentTab.id},
     function: getSource
    }).then(results => {
     var source = results[0];
     var formData = new FormData();
     formData.append('file-data', new Blob([source.result], {type: 'text/html'}), 'source.html');
     fetch('https://timetable.learnabroad.cn/timetable', {
      method: 'POST',
      body: formData
     }).then(response => response.blob())
      .then(blob => {
       var url = URL.createObjectURL(blob);
       var a = document.createElement('a');
       a.href = url;
       a.download = 'timetable.ics';
       document.body.appendChild(a);
       a.click();
       document.body.removeChild(a);
      });
    });
   });
  });
  
  function getSource() {
   return document.documentElement.outerHTML;
  }
  